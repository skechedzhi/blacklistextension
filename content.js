
blacklistResolve = function (list) {
    if (list && list.length) {
        var regexp = new RegExp(list);

        // if site is blacklisted - add a warning Header to a Page
        if (regexp.test(window.location.href)) {
            var sign = document.createElement('div');
            sign.className = 'blacklist-banner';

            var h1 = document.createElement('h1');
            h1.innerText = chrome.i18n.getMessage('Banner_Header');

            var expl = document.createElement('span');
            expl.innerText = chrome.i18n.getMessage('Banner_Text');

            var img = document.createElement('img');
            img.src = chrome.extension.getURL('trash.jpg');

            var optionsLink = document.createElement('a');
            optionsLink.innerText = chrome.i18n.getMessage('Edit');
            optionsLink.className = 'options-edit-link';
            optionsLink.href = chrome.extension.getURL("options/options.html");
            optionsLink.onclick = function () {window.open(this.href); return false;};

            sign.appendChild(img);
            sign.appendChild(h1);
            sign.appendChild(expl);
            sign.appendChild(optionsLink);

            document.body.insertBefore(sign, document.body.firstChild);
        } else {
            // if site is good, check all it`s links for Black List site references
            var links= document.getElementsByTagName('a');
            for (var i = 0; i < links.length; i++) {
                if (regexp.test(links[i].href)) {
                    shitIt(links[i]);
                }
            }

            chrome.storage.sync.get({monitorDOM:true}, function (stored) {
                if (stored.monitorDOM) {

                    // listen to DOM changes to mark ajax fetched links
                    (new MutationObserver(function (mutations) {
                        mutations.forEach(function (mutation) {
                            if (mutation.addedNodes && mutation.addedNodes.length) {
                                mutation.addedNodes.forEach(function (node) {
                                    if (node.tagName == 'A' && regexp.test(node.href)) {
                                        shitIt(node);
                                    } else if (node.childNodes.length) {
                                        var links = node.getElementsByTagName('a');
                                        for (var i = 0; i < links.length; i++) {
                                            if (regexp.test(links[i].href)) {
                                                shitIt(links[i]);
                                            }
                                        }
                                    }
                                });
                            }
                        });
                    })).observe(document.getElementsByTagName('body')[0], {
                        childList: true,
                        subtree: true
                    });
                }
            });
        }
    }
};

function shitIt(link) {
    var trashIcon = document.createElement('img');
    trashIcon.src = chrome.extension.getURL('trash_icon.png');
    trashIcon.title = 'Link to a Trash Site!';
    trashIcon.height = 32;
    trashIcon.width = 32;

    link.parentNode.insertBefore(trashIcon, link.nextSibling);
}

// request Black List
chrome.runtime.sendMessage({
    to:    'storage',
    action: 'requestBlackList'
}, blacklistResolve);
