# README #

Chrome Extension Extension marks information portal as a Trash Source 
based on the blacklist provided by http://boloto.in.ua and www.stopfake.online

## Release Notes 1.4.6 ##

* Change Manifest - reduced permissions request to follow Google policy

## Release Notes 1.4.6 ##

* https for stopfake
* bug fixing

## Release Notes 1.4.5 ##

* Code refactor

## Release Notes 1.4.4 ##

* added Last Sync Date
* code refactored
* bugs fixed

## Release Notes 1.4.3 ##

* bugs fixed

## Release Notes 1.4.2 ##

* stopfake enabled by default

## Release Notes 1.4.1 ##

* added possibility to sync with multiple sources
* added www.stopfake.online as a second source
* FF version fixes
* bug fixes

## Release Notes 1.3.5 ##

* possibility to add sites to local Black List
* FF version fixes
* bug fixes
* Edit link on alert banner

## Release Notes 1.3.1 ##

* added Ukrainian locale
* monitorDOM is now enabled by default

## Release Notes 1.3.0 ##

* added periodicaly synchronization with remote black list

## Release Notes 1.2.0 ##

* added multilanguage support
* added RU Language translation 

### setup in dev mode ###

* download zip
* unpack
* enter 'chrome://extensions/' in chrome`s address string
* Check 'developer mode' checkbox
* click 'upload unpacked extension'
* select a folder with extension
