//################## TOOLS ###################\\

// Warn if overriding existing method
if(Array.prototype.equals)
    console.warn("Overriding existing Array.prototype.equals. Possible causes: New API defines the method, there's a framework conflict or you've got double inclusions in your code.");
// attach the .equals method to Array's prototype to call it on any array
Array.prototype.equals = function (array) {
    // if the other array is a falsy value, return
    if (!array)
        return false;

    // compare lengths - can save a lot of time
    if (this.length != array.length)
        return false;

    for (var i = 0, l=this.length; i < l; i++) {
        // Check if we have nested arrays
        if (this[i] instanceof Array && array[i] instanceof Array) {
            // recurse into the nested arrays
            if (!this[i].equals(array[i]))
                return false;
        }
        else if (this[i] != array[i]) {
            // Warning - two different object instances will never be equal: {x:20} != {x:20}
            return false;
        }
    }
    return true;
};
// Hide method from for-in loops
Object.defineProperty(Array.prototype, "equals", {enumerable: false});

//###############################################################\\


var binding = {
    apply: function (data) {
        var value;
        var bindings = document.querySelectorAll('[bind]');

        for (var i = 0; i < bindings.length; i++) {
            var model = data[bindings[i]['attributes']['bind']['value']];
            if (typeof model != 'undefined') {
                if (bindings[i]['attributes']['bind-value']) {
                    if (typeof model.indexOf != 'undefined') {
                        // this is array
                        value = model.indexOf(bindings[i]['attributes']['bind-value']['value']) != -1;
                    } else {
                        // this is object
                        value = model[bindings[i]['attributes']['bind-value']['value']];
                    }
                } else {
                    value = model;
                }

                if (bindings[i].type === 'checkbox') {
                    bindings[i].checked = value;
                } else if (bindings[i].type === 'text') {
                    bindings[i].value = value;
                } else {
                    // not an input
                    if (typeof value != 'undefined') {
                        if (typeof bindings[i]['attributes']['bind-target'] != 'undefined') {
                            var target = bindings[i].querySelector(bindings[i]['attributes']['bind-target']['value']);
                            target.innerText = value;
                        } else {
                            bindings[i].innerText = value;
                        }
                    } else if (typeof bindings[i]['attributes']['bind-cloak'] != 'undefined') {
                        bindings[i].style.display = 'none';
                    }
                }
            }
        }
    },

    collect: function (data) {
        data = data || {};
        var bindings = document.querySelectorAll('input[bind]');
        for (var i = 0; i < bindings.length; i++) {
            var model = data[bindings[i]['attributes']['bind']['value']];

            if (bindings[i]['attributes']['bind-value']) {
                if (typeof model == 'undefined') {
                    model = [];
                }
                if (bindings[i]['checked']) {
                    model.push(bindings[i]['attributes']['bind-value']['value']);
                }
            } else {
                model = bindings[i]['checked'];
            }

            data[bindings[i]['attributes']['bind']['value']] = model;
        }

        return data;
    }
};

// Saves options to chrome.storage.sync.
function saveOptions() {
    chrome.storage.sync.get({sources:[]}, function (oldValues) {
        // get selected checkboxes
        var data = binding.collect();

        // save data
        chrome.storage.sync.set(data, function () {
            // Update status to let user know options were saved.
            var status = document.getElementById('status');
            status.innerHTML = '<span style="color:green">Options saved.</span>';

            if (!data.sources.equals(oldValues.sources)) {
                    syncOptions();
                } else {
                    setTimeout(function () {
                        status.innerHTML = '&nbsp;';
                        chrome.runtime.reload(); //Reload extension after options save
                    }, 1000);
                }
            }
        );
    });
}

// Restores select box and checkbox state using the preferences stored in chrome.storage.
function loadOptions() {
    chrome.storage.sync.get(
        {
            list: [],
            remoteList: [],
            localList: [],
            monitorDOM: true,
            sources: ['boloto', 'stopfake'],
            lastSyncDate : {},
            lastSyncState : {}
        },
        function (storage) {
            // FF seams does not support default values on first request
            if (!storage) {
                storage = {
                    list: [],
                    remoteList: [],
                    localList: [],
                    monitorDOM: true,
                    sources: ['boloto']
                };
            }

            var container = document.getElementById('list');
            container.innerHTML = '';

            // render remote list
            storage.remoteList.forEach(function (item) {
                var row = createOptionRow(item, 'list');
                container.appendChild(row);
            });

            // render local list
            storage.localList.forEach(function (item) {
                var row = createOptionRow(item, 'localList');
                container.appendChild(row);
            });

            // set checkbox states
            binding.apply(storage);
        }
    );
}

function syncOptions() {
    chrome.storage.sync.set({ list:false, remoteList: [] }, function () {
        // Update status to let user know options were saved.
        var status = document.getElementById('status');
        status.innerHTML = '<span style="color:blue">Synchronization ...</span>';

        chrome.runtime.sendMessage({
            to: 'storage',
            action: 'syncBlackList'
        }, function (list) {
            var status = document.getElementById('status');
            var msg = 'Synchronization done. ' + (list ? list.length : 0) + ' items loaded. (reload page to see changes)';
            status.innerHTML = '<span style="color:green">' + msg +'</span>';

            setTimeout(function (args) {
                chrome.runtime.reload();
            }, 1000); //Reload extension after options save
        });
    });
}

/**
 * Creates a BlackList Option row (checkbox)
 *
 * @param url
 * @param bind
 * @param checked
 * @returns {Element}
 */
function createOptionRow(url, bind, checked) {
    var row = document.createElement('li');
    var input = document.createElement('input');
    input.type = 'checkbox';
    input.setAttribute('bind', bind);
    input.setAttribute('bind-value', url);

    if (typeof checked != 'undefined') {
        input.checked = checked;
    }

    var text = document.createElement('span');
    text.innerText = url;

    row.appendChild(input);
    row.appendChild(text);

    return row;
}

function addLink(e) {
    if (e.keyCode == 13 && e.target.value && e.target.value.length) { // Enter

        var validateUrl = /(?:(?:[a-z\d](?:[a-z\d-]*[a-z\d])*)\.)+[a-z]{2,}/;

        if (!validateUrl.test(e.target.value)) {
            document.getElementById('add_link_error').style.display = 'block';
        } else {
            document.getElementById('add_link_error').style.display = 'none';

            var row = createOptionRow(e.target.value, 'localList', true);
            document.getElementById('list').appendChild(row);

            e.target.value = '';

            // Update status.
            var status = document.getElementById('status');
            status.innerHTML = '<span style="color:red">Save options to store your changes.</span>';
        }
    }
}


(function translate() {
    document.querySelectorAll('[data-message]').forEach(function (el) {
        el.innerText = chrome.i18n.getMessage(el.dataset.message);
    });
})();

document.getElementById('locale').innerText = chrome.i18n.getUILanguage();

document.addEventListener('DOMContentLoaded', loadOptions);
// document.getElementById('debug').addEventListener('click', loadOptions); // debug
document.getElementById('save').addEventListener('click', saveOptions);
document.getElementById('resync').addEventListener('click', syncOptions);
document.getElementById('add_link').addEventListener('keyup', addLink);
