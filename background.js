var processors = {
    storage: {
        /**
         * @param data (array)
         * @param sendResponse (callable)
         * @return string
         */
        requestBlackList: function (data, sendResponse) {
            if (!models.blackList.listString) {
                chrome.storage.sync.get({list: false, localList: []}, function (storedList) {
                    // for FF
                    if (!storedList) {
                        storedList = {list: false, localList: []};
                    }
                    if (!storedList.list) {
                        processors.storage.syncBlackList(data, function (list) {
                            models.blackList.setList(list.concat(storedList.localList));
                            sendResponse(models.blackList.listString);
                        });
                    } else {
                        models.blackList.setList(storedList.list.concat(storedList.localList));
                        sendResponse(models.blackList.listString);
                    }
                });
            } else {
                sendResponse(models.blackList.listString);
            }
        },

        /**
         *
         * @param data (array)
         * @param response (callable)
         * @return array
         */
        syncBlackList: function (data, response) {
            chrome.storage.sync.get(
                {
                    sources: ['boloto', 'stopfake'],
                    list: false,
                    remoteList: [],
                    lastSyncDate: {}
                },
                function (storedData) {
                    // for FF
                    if (!storedData) {
                        storedData = {sources: ['boloto', 'stopfake'], list: false, remoteList: [], lastSyncDate: {}};
                    }

                    var loadedList = [];
                    var lastSyncDate = {};
                    var adaptersInProcess = 0;

                    function done() {
                        if (adaptersInProcess === 0) {
                            if (loadedList.length) {
                                storedData.remoteList = loadedList;

                                if (!storedData.list) {
                                    storedData.list = loadedList;
                                }

                                Object.assign(storedData.lastSyncDate, lastSyncDate);

                                chrome.storage.sync.set(storedData, function () {
                                    response(loadedList);
                                });
                            } else {
                                response([]);
                            }
                        }
                    }

                    storedData.sources.forEach(function (adapterName) {
                        if (typeof adapters[adapterName] != 'undefined') {
                            adaptersInProcess++;

                            // iterate next adapter
                            models.blackList.loadList(adapters[adapterName], function (list) {
                                    loadedList = loadedList.concat(list);
                                    lastSyncDate[adapterName] = Date();
                                    adaptersInProcess--;
                                    done();
                                }
                            );
                        }
                    });
                }
            );
        }
    }
};


var models = {
    blackList: {
        listString: false,
        setList: function (listArray) {
            var list = listArray.join('|');
            models.blackList.listString = list.replace(/([\.\-\_])/g, "\\$1");
        },
        loadList: function (adapter, respond) {
            console.log('started sync: ' + adapter.syncUrl);
            var xhr = new XMLHttpRequest();
            xhr.open("GET", adapter.syncUrl, true);
            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4) {
                    if (xhr.responseText.length) {
                        try {
                            var html = document.createRange().createContextualFragment(xhr.responseText);
                            var list = adapter.parse(html);
                        } catch (e) {
                            console.log(e);
                            list = [];
                        }
                        console.log(list);
                        respond(list);

                    } else {
                        console.log('No Content');
                        respond([]);
                    }
                }
            };
            xhr.send();
        }
    }
};

var adapters = {
    boloto: {
        syncUrl: 'http://boloto.in.ua',
        parse: function (html) {
            var list = [];

            var listItems = html.querySelectorAll('[id^=demo]');
            if (listItems.length) {
                listItems.forEach(function (li) {
                    list.push(li.innerText);
                });
            } else {
                var script = html.querySelector('div.example script');
                if (script) {
                    listItems = /new Array.?\((.*?)\)/.exec(script.innerText.replace(/\n/g, " "))[1].match(/'.*?'/g);
                    listItems.forEach(function (elText) {
                        list.push(elText.replace(/'| /g, ''));
                    });
                }
            }

            return list;
        }
    },

    stopfake: {
        syncUrl: 'https://www.stopfake.online/p/blog-page.html',
        parse: function (html) {
            var list = [];

            var listItems = html.querySelectorAll('ul[id^=myUL] li a');
            if (listItems.length) {
                var validateUrl = /(?:(?:[a-z\d](?:[a-z\d-]*[a-z\d])*)\.)+[a-z]{2,}/;

                var headers = 0;
                listItems.forEach(function (link) {
                    // count header elements
                    if (link.className == 'header' && link.innerText) {
                        headers++;
                        return; // bypass header element
                    }

                    if (headers == 1) { // after first header
                        if (validateUrl.test(link.innerText)) {
                            list.push(link.innerText);
                        }
                    }

                });
            }

            return list;
        }
    }
};

// Router
chrome.runtime.onMessage.addListener(function (msg, sender, sendResponse) {
    if (msg.to && msg.action) {
        try {
            var model = processors[msg.to];
            if (model && typeof model[msg.action] == 'function') {
                model[msg.action](msg, sendResponse);
            }
        } catch (e) {
            console.log(e);
        }

        return true; // activate async response
    }
});

// Cron
chrome.alarms.create('blacklist_remote_sync', {
    delayInMinutes: 1440, // day
    periodInMinutes: 10080 // every week
});

chrome.alarms.onAlarm.addListener(function (alarm) {
    if (alarm.name == 'blacklist_remote_sync') {
        console.log('Synchronization...');

        processors.storage.syncBlackList({}, function (list) {
            console.log('Loaded ' + list.length + ' websites');
            chrome.runtime.reload(); //Reload extension
        });
    }
});

//On Update
// Check whether new version is installed
chrome.runtime.onInstalled.addListener(function (details) {
    if (details.reason == "update") {
        var thisVersion = chrome.runtime.getManifest().version;
        if (thisVersion == '1.4.3' && details.previousVersion != '1.4.3') {
            chrome.storage.sync.set({list: false, remoteList: []}, function () {
                processors.storage.syncBlackList({}, function () {
                    chrome.runtime.reload();
                });
            });
        }
    }
});
